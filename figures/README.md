Here we illustrate the use Guix to produce graphical informations.

# Setting up the environment

The environment can be reproduced with the files: `channels.scm` (revision)
and `manifest.scm` (package) from this directory. All softwares and libraries
will be provided in the environment after running the command lines below:
```
cd your_path_to_git/guixreprodsci/figures
guix time-machine -C channels.scm -- environment -m manifest.scm
```

# Use Inkscape in a reproducible environment

Open inkscape to access `.svg` file.  Run `inkscape` then open `Figure1.svg`.

# Reproducible violin plots figure from R script

Run `R` then `source("Figure1_source.R")`.

# Build the dependency graph of `R`

Full size of the directed acyclic graph (DAG )of `R` dependencies shown in Figure 1
can be obtained with `guix graph` which produces a `graphviz` formated output
that can be used to produce a figure with `dot`.

```
# To get a figure
guix graph r-minimal | dot -Tsvg > r-minimal.svg
# To get a plain text
guix graph r-minimal | dot -Tplain > r-minimal.txt
# To get a plain text of all dependancies from origins
guix graph r-minimal --list=bag-with-origins | dot -Tplain > r-minimal.txt
```

