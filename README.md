# Guix for reproducible research

This repository describes additional information on methods to allow
reproducibility of computational analyses.

## Install Guix and reproduce the environement

Guix is running on the top of any Linux distribution and it does not
interfere.  The only minimal requirement is a Linux kernel version 3.19+.

The project provides a script which needs `root` privileges:
```
cd /tmp
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
chmod +x guix-install.sh
./guix-install.sh
```
Follow the instructions.  A GPG public key is required and the script will ask
you a confirmation for processing for you.  Last, do not miss to allow
pre-built binary
[substitutions](https://guix.gnu.org/manual/devel/en/guix.html#Substitutes).

We recommend, right after this installation, to
[upgrade](https://guix.gnu.org/manual/devel/en/guix.html#Upgrading-Guix).  As
`root`, run:
```
guix pull
```
then restart the daemon depending on your Linux distribution; for instance on
Ubuntu or Debian, it reads:
```
systemctl restart guix-daemon.service
```

Once installed, you can check the Guix revision as a regular user,
```
guix describe
```
(The Guix revision as regular user is independent to the Guix revision as
privileged user `root`.  Other said, a regular user can use any Guix revision,
and the guix-daemon is stable enough to ensure stability there.)

Or also build a simple package,

```
guix install hello
```

More detailed procedure can be found in the [Guix
manual](https://guix.gnu.org/manual/en/html_node/Binary-Installation.html),
especially the [Application
Setup](https://guix.gnu.org/manual/devel/en/guix.html#Application-Setup)
section.


## Open source example

The `opensource_example` folder stores scripts and pedestrian view to
reproduce computational analyses from Ma CY et al. Stimulation strength
controls the rate of initiation but not the molecular organisation of
TCR-induced signalling. eLife
2020;9:e53948. https://elifesciences.org/articles/53948

We are very grateful to the journal and the authors.  They are respecting good
practises, from data to script availability, to allow us to rerun with minor
effort.

We detail in the folder `opensource_example` what we are identifying as
roadblocks for improving the situation about redoing analyses.  We pick this
paper because it is already providing good standards for reproducing.

We let aside the number of papers we have explored.  They fails at various
levels: availability of data, scripts, software tools, etc.

## Figures

The `figures` folder stores instructions to reproduce graph dependencies
diagrams and figures used to draw Figure 1.

## User Interface

From the user interface point of view, knowledge on command line interactions
is required. This is counterbalanced by the fact that inputs in interactive
user-friendly interface cannot be reproduced, or hardly. Users can find help
in tutorials and tools developed by an [active
community](https://hpc.guix.info). Furthermore, the [Guix
manual](https://guix.gnu.org/en/manual/en/guix.html) is well documented and
`help` command allow users to find their way through all subcommands.
