# Open source and data example

To illustrate how Guix may be used to reproduce computational bench, we use a
fully open access, open data, open source research article.

## Roadblocks

We are very grateful to the online journal and the authors of the paper we are
using for the demonstration; they are respecting good practises, from data to
script availability, to allow us to rerun with minor effort.  If all was as
such quality, then reproducible research would be easier.

Here, we underline the issue we had and what could be improved.  It is
twofold: data and source code script.

 1. Data:
    - Fetching them requires too many manual steps.  Here, only two set is
      required which is affordable but the number of set is often larger.
    - No integrity check.  No guarantee that for instance FR-FCM-Z2CP had not
      been changed after the publication.

  => Data requires intrinsic referencing with well-documented public API.  For
  instance, service as [Zenodo](https://zenodo.org/) hosted by CERN would
  help.

  2. Scripts:
     - The paper does not describe all the packages we had to include for
       reproducing.
     - The analyses uses the proprietary software FlowJo for pre-processing
       the data.  On one hand, it would be better to disclose all the
       procedure, on the other hand, such pre-processing could be considered
       as part of the experimental materials (where many are patented and
       closed).
     - The paper refers to a Git repository but not any commit.  To be
       concrete, the repository holds 2 commits from March 2020; which could
       be ambiguous.
     - The archived repository is on the same platform (GitHub) as the
       original code.  Archiving on the same forge platform does not guarantee
       a strong authority, for both the long-term avaibility and a potential
       malicous replacement.

  => Source code requires intrinsic referencing on archive-focused plateform
  as [Software Heritage](https://www.softwareheritage.org/)

  => Share reproducible research requires tools that describe the complete
  computational environment.

## Data availability

### Article results: eLife

Ma CY et al. «Stimulation strength controls the rate of initiation but not the
molecular organisation of TCR-induced signalling». eLife 2020;9:e53948.
https://elifesciences.org/articles/53948

### Source code

- The source code can be cloned from this repository:
  https://github.com/MarioniLab/SignallingMassCytoStimStrength
  commit: d85402f3d951edf2c51281e3d09ea96a5c7da612
- An archived version can founded here:
  https://archive.softwareheritage.org/swh:1:dir:192f30f23023ac72a78a071996a51d4afc67be3c

```
git clone https://github.com/MarioniLab/SignallingMassCytoStimStrength
cd SignallingMassCytoStimStrength
git checkout d85402f3d951edf2c51281e3d09ea96a5c7da612
guix hash -rx .

```
The last command computes the checksum
1sa6x1qvilazlhwhry97qm0fkc0dxrkzc98maflgcm586banbpx7.  This checksum will be
exactly the same using the Software Heritage download vault.

### Data: Flow Repository

Make sure to download `.fcs` files in a the folder `/home/my-path-to-git-repo/data`

- FR-FCM-Z2CP: https://flowrepository.org/id/FR-FCM-Z2CP
- (optionally) FR-FCM-Z2CX: https://flowrepository.org/id/FR-FCM-Z2CX

## Build a reproducible environment with Guix

### Capture the environment

In the file `channels.scm` we describe the revision, a Git repository pointing
to a specific commit, that Guix will call to use the exact same binaries in
order to populate the computional environment with the required software and
libraries.

### Required software

In the file `manifest.scm`, we describe software (R libraries) used for the
analyses.  Note that the package `r-cydar` is not known by Guix proper; the
package `r-cydar` is part of the package collection that Guix provides by
default.  We thus need to extend the package collection and describe by
ourselves the recipe to build this package from its sources host on
Github. This recipe can be found within the file `my-pkgs.scm` in the
directory `my-pkgs` – note that Guix provides facilities to obtain such
definition, see [`guix import
cran`](https://guix.gnu.org/manual/devel/en/guix.html#Invoking-guix-import).

## Deploy the environment

### Run locally

Now we are ready to reproduce the environement. Run the following command:
```
guix time-machine -C channels.scm -- environment -m manifest.scm -L my-pkgs -- R
```
and redo some markdown (`.Rmd`) output by running the following command:
```
rmarkdown::render(Timecourse_peptides_analysis.Rmd)
```

### Create a container and share

```
IMG=$(guix time-machine -C channels.scm      \
           -- pack -f docker -m manifest.scm \
           -L my-pkgs                        \
           -C none                           \
           -S /bin=bin                       \
           -S /lib=lib                       \
           -S /share=share                   \
           -S /etc=etc                       \
           --save-provenance)
```

```
docker load < $IMG
docker images
docker tag ec06ad3da7ba  zimoun/guixreprosci:compute-v1
docker login
docker push zimoun/guixreprosci:compute-v1
```
where ec06ad3da7ba correspond to <IMAGE-ID> fromt the just imported image.

### Run wihout Guix installed via Docker pack

Assuming you fetched the data and the script, running the command:

```
docker run -v `pwd`:`pwd` -w `pwd` -ti zimoun/guixreprosci:compute-v1 \
       Rscript -e "rmarkdown::render('Timecourse_peptides_analysis.Rmd')"
```
will produce the analysis.

### Example of transparent Docker images

We are providing two Docker pack images: one used for the computations only
(see above) and the other for fetching and preparing the data.  They are both
built the same way.  Here, we do not provide the recipe–let mention the
packages `git`, `nss-certs`, `coreutils`, `bash`, `xz`, `r-minimal` and
`r-flowcore` at the revision described by `channels.scm`–for the later pack
pushed to the public Docker registry because we want to point two main issues:

 1. If the binary image located in the public Docker registry
    `zimoun/guixreprosci:prepare-v1` disappears, then one would have to recreate
    it.  We are challenging everyone to use a `Dockerfile`, build today the
    image, wait one year, then try to rebuild this image using this
    `Dockerfile`, last compare both images.

 2. The format Docker for the container pack must not be flustered with the
    way (`Dockerfile`) for producing it.  The use of binary containers as
    Docker images eases a quick share.  And we are claiming that producing
    such images with `Dockerfile` will suffer the same issues as the
    underlined system it is based on and nothing about transparency and
    reproducible research is fondamentally changed, only switched a bit.

Last, if this image `zimoun/guixreprosci:prepare-v1` is still available, then
you can extract the extract the recipe for reproducing it by following the
[steps
here](https://hpc.guix.info/blog/2021/10/when-docker-images-become-fixed-point/).

### Running on foreign infrastructure without Guix

We provide an example using GitlabCI, see the `.gitlab-ci.yml` at the root of
this repository for details.

However, because the size `artifacts` are limited between stages and because
memory RAM limits, we apply a downsampling to reduce their size.  It is only
for the demonstration using the public GitlabCI runners which limits the
resources.  However, it locally works (see above) or please use your own
GitlabCI instance.

The generated output of the downsampled data is available
[here](http://nivall.gitlab.io/guixreprodsci).

For completeness and comparison, we provide the file
`Timecourse_peptides_analysis.html` corresponding the raw data.
