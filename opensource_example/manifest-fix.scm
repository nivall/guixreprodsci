(use-modules (guix packages)
             (ice-9 match))

(define bioconductor-url
  "bioconductor.org")
(define bioconductor-release
  (@ (guix import cran) %bioconductor-version))

(define (bioconductor? p)
  (match (package-source p)
    ((? origin? o)
     (match (origin-uri o)
       ((url rest ...)
        (string-contains url bioconductor-url))
       (_ #false)))
    (_ #false)))

(define (package-seen? pkg)
  (assq-ref (package-properties pkg) 'bioconductor))

(define (extend-url pkg)
  (cond
   ;; Already processed
   ((package-seen? pkg) pkg)
   ;; Process if it comes from Bioconductor
   ((bioconductor? pkg)
    (let ((src (package-source pkg)))
      (package
        (inherit pkg)
        (source
         (origin
           (inherit src)
           (uri (append
                 (origin-uri src)
                 (list (string-append
                        "https://" bioconductor-url "/packages/"
                        bioconductor-release "/bioc/src/contrib/"
                        (or
                         (assq-ref (package-properties pkg) 'upstream-name)
                         (package-name pkg))
                        "_"
                        (package-version pkg) ".tar.gz"))))))
        (properties `((bioconductor . #true)
                      ,@(package-properties pkg))))))
   ;; Do nothing for all the other packages
   (else pkg)))

(define (cut? pkg)
  (or (not (bioconductor? pkg))
      (package-seen? pkg)))

(define fix-bioconductor-url
  (package-mapping extend-url cut?))

(define with-r-codetools-bis
  (package-input-rewriting
   `((,(specification->package "r-codetools")
      .
      ,(specification->package "r-codetools-bis")))))

(packages->manifest
 (map
  (compose
   fix-bioconductor-url
   with-r-codetools-bis
   specification->package)
  (list
   ;; Packages from Guix collection
   "r"
   "r-rtsne"
   "r-pheatmap"
   "r-rcolorbrewer"
   "r-ncdfflow"
   "r-edger"
   "r-flowcore"
   "r-dplyr"
   "r-combinat"
   "r-rmarkdown"                         ;render .Rmd files

   ;; Extend collection by defining in folder my-pkgs/my-pkgs.scm
   "r-cydar"

   ;; Only for Docker GitLabCI
   "coreutils"
   "bash"
   )))
