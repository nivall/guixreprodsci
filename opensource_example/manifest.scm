(specifications->manifest
 (list
  ;; Packages from Guix collection
  "r"
  "r-rtsne"
  "r-pheatmap"
  "r-rcolorbrewer"
  "r-ncdfflow"
  "r-edger"
  "r-flowcore"
  "r-dplyr"
  "r-combinat"
  "r-rmarkdown"                         ;render .Rmd files

  ;; Extend collection by defining in folder my-pkgs/my-pkgs.scm
  "r-cydar"

  ;; Only for Docker GitLabCI
  "coreutils"
  "bash"
  ))
