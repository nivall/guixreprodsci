(define-module (my-pkgs-fix)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module ((gnu packages statistics) #:select (r-codetools)))

(define-public r-codetools-bis
  (package
    (inherit r-codetools)
    (name "r-codetools-bis")
    (source
     (origin
         (method url-fetch)
         (uri
          "http://cran.r-project.org/src/contrib/Archive/codetools/codetools_0.2-16.tar.gz")
         (sha256
          (base32 "1dklibnp747a0p41ggcf8fyw36xhj9c869gay80ggfns79y7axn2"))))))
