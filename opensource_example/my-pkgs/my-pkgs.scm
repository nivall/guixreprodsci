;;;
;; This file define the recipe to build cydar which is not upstream.
;; Here cydar source is fetched from GitHub instead of Bioconductor.
;;
;;; Note:
;; Building the package may take some times.

(define-module (my-pkgs)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system r)
  #:use-module (guix licenses)
  #:use-module (gnu packages bioconductor)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages statistics))

(define-public r-cydar
  (let ((commit "ee47526a825cb441ca9c5f259382e32488b2250b")
        (revision "1"))
    (package
      (name "r-cydar")
      (version (git-version "1.15.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/MarioniLab/cydar")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1xr0sbipz5y2rjdqis7x3fbx02dilwnvy5c30hfk72kmf92y29bp"))))
      (properties `((upstream-name . "cydar")))
      (build-system r-build-system)
      (propagated-inputs
       `(("r-biobase" ,r-biobase)
         ("r-biocgenerics" ,r-biocgenerics)
         ("r-biocneighbors" ,r-biocneighbors)
         ("r-biocparallel" ,r-biocparallel)
         ("r-flowcore" ,r-flowcore)
         ("r-rcpp" ,r-rcpp)
         ("r-s4vectors" ,r-s4vectors)
         ("r-shiny" ,r-shiny)
         ("r-singlecellexperiment" ,r-singlecellexperiment)
         ("r-summarizedexperiment" ,r-summarizedexperiment)
         ("r-viridis" ,r-viridis)))
      (native-inputs `(("r-knitr" ,r-knitr)))
      (home-page "https://github.com/MarioniLab/cydar")
      (synopsis "Using Mass Cytometry for Differential Abundance Analyses")
      (description
       "Identifies differentially abundant populations between samples and
groups in mass cytometry data.  Provides methods for counting cells into
hyperspheres, controlling the spatial false discovery rate, and visualizing
changes in abundance in the high-dimensional marker space.")
      (license gpl3))))
