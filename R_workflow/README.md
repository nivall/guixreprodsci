# How to create a reproducible R environment from scratch?

Here we describe a step-by-step approach on how to implement Guix in a `R` workflow. 

# Define the required packages and software: the manifest file

Consider that we need to draw a plot using `R` with the packages `ggplot2` and `RColorBrewer`. The first thing we need is to create the file which describes these libraries. This file may be called `manifest.scm`. The `R` package names start with `r-`. To check if a package or software is already known by `Guix` one can write 

```
Guix show r-<name-of-the-package>
```

Else, complete list of integrated packages in `Guix` can be found [on Guix website](https://guix.gnu.org/en/packages/) or [on Guix-HPC website](https://hpc.guix.info/browse).

We checked that `R` (`r-minimal`), `ggplot2` and `RColorBrewer` were already indexed and we have created the `manifest.scm` file with a text editor and write the following lines:

```
(specifications->manifest
 (list
  "r-minimal"
  "r-ggplot2"
  "r-rcolorbrewer"
  ))
```

Note that the manifest file is not mandatory, see below a more direct method.

# Save the state of the dependencies: the channel file

We have defined the software that need to be build for our analyses. Since we want the binaries to be reproduced byte-to-byte, we have to define the dependencies on which these will be built. We can either use the `channels.scm` file from a previous project or create a new one. We can copy-paste the output from the following code line:

```
<COPY-PASTE METHOD>
guix describe --format=channels
<OR DIRECTLY SAVED>
guix describe --format=channels > channels.scm
```

# Reproduce your computational environment: guix time-machine

Now that our `manifest.scm` and `channels.scm` files are ready we can run the following command on the terminal and the R console will be available with the packages required:

```
guix time-machine -C channels.scm -- environment -m manifest.scm --R
```

If we don't have the `manifest.scm` file we can directly specify the packages to use:

```
guix time-machine -C channels.scm -- environment r-minimal r-ggplot2 r-rcolorbrewer -- R
```

# What is the package is not upstream?

If the package is not found by `guix show r-<name-of-the-package>`, it can be used imported and used following the procedure described in [Guix manual](https://guix.gnu.org/manual/en/html_node/Invoking-guix-import.html). An example of a package not upstream is shown in the `opensource_example` folder with `r-cydar`.
